/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package penjualan;

import barang.Mobil;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author budi
 */
public class PenjualanMobil {
    private double modal = 10000000000.0;
    private double nilaiAsset = 0.0;
    
    private List<Mobil> daftarMobil;

    public PenjualanMobil() {
        daftarMobil = new ArrayList<>();
    }

    public void beliMobil(Mobil mobil) {
        daftarMobil.add(mobil);
        modal -= mobil.getHarga();
        nilaiAsset += mobil.getHarga();
    }

    public void jualMobil(Mobil mobil, double harga) {
        daftarMobil.remove(mobil);
        modal += harga;
        nilaiAsset -= mobil.getHarga();
    }

    public List<Mobil> cariMobilBerdasarkanMerek(String merek) {
        List<Mobil> hasilPencarian = new ArrayList<>();
        for (Mobil mobil : daftarMobil) {
            if (mobil.getMerek().equalsIgnoreCase(merek)) {
                hasilPencarian.add(mobil);
            }
        }
        return hasilPencarian;
    }
}
